if [ -e /etc/environment ]; then
    source /etc/environment
fi

# start fish shell
WHICH_FISH="$(which fish)"
if [[ "$-" =~ i && -x "${WHICH_FISH}" && ! "${SHELL}" -ef "${WHICH_FISH}" ]]; then
  # Safeguard to only activate fish for interactive shells and only if fish
  # shell is present and executable. Verify that this is a new session by
  # checking if $SHELL is set to the path to fish. If it is not, we set
  # $SHELL and start fish.
  #
  # If this is not a new session, the user probably typed 'bash' into their
  # console and wants bash, so we skip this.
  exec env SHELL="${WHICH_FISH}" "${WHICH_FISH}" -i
fi

# This is probably overkill. Cute overkill. I like overkill.
source ~/.config/zsh/aliases.zsh
source ~/.config/zsh/colors.zsh
source ~/.config/zsh/prompt.zsh
source ~/.config/zsh/completions.zsh
source ~/.config/zsh/path.zsh
source ~/.config/zsh/keys.zsh
source ~/.config/zsh/history.zsh

# Configurations that should not be used at home (they wouldn't work anyway).
work_options="$HOME/.config/zsh/work.zsh"
if [[ -e $work_options ]]; then
    source $work_options
fi

setopt extendedglob
unsetopt beep

# Use fzf, if available.
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# I don't always want to use this.
function conda-init() {
    # >>> conda initialize >>>
    # !! Contents within this block are managed by 'conda init' !!
    __conda_setup="$('/home/lerm/miniconda3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
    if [ $? -eq 0 ]; then
        eval "$__conda_setup"
    else
        if [ -f "/home/lerm/miniconda3/etc/profile.d/conda.sh" ]; then
            . "/home/lerm/miniconda3/etc/profile.d/conda.sh"
        else
            export PATH="/home/lerm/miniconda3/bin:$PATH"
        fi
    fi
    unset __conda_setup
    # <<< conda initialize <<<
}

