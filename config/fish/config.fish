if status is-interactive
    # Commands to run in interactive sessions can go here
    fish_vi_key_bindings

    source ~/.config/shell/aliases.sh
end

fish_add_path $HOME/bin

# Created by `pipx` on 2023-06-05 16:07:02
set PATH $PATH /home/lerm/.local/bin
