# This file should work in all of bash, zsh, and fish, so only simple aliases
# (no function definitions) should be used.

# I'm really tired of typing this wrong.
alias ç=l
alias çç=ll

# Examine files in the console with colored output.
alias ccat='pygmentize -g'

# Git aliases.
alias gs='git status'
alias ga='git add'
alias gc='git commit'

# Mercurial aliases.
alias hx='hg x'
alias hs='hg status'

# Man, I'm lazy.
alias sapt='sudo apt-get'

# Up one directory.
alias uu='cd ..'
# Back to the old directory.
alias bd='cd $OLDPWD'
# Push current directory on stack.
alias sd='pushd $PWD'

# Sane defaults for du, similar to the standard with ls.
alias du='du -h'

# Case-insensitive searches by default with less.
alias less='less -i'

# Some usefull shortcuts.
alias oo='xdg-open'

# This is so I don't have to remember the correct parameters to sshfs. This
# should allow git repositories to work with a mounted filesystem.
alias mountssh='sshfs -o workaround=rename'

# Escape accents (useful for bibtex)
alias latexcape='recode -d u8..ltex'

# Dbus commands for poweroff and friends.
alias dbus-shutdown='dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 "org.freedesktop.login1.Manager.PowerOff" boolean:true'
alias dbus-reboot='dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 "org.freedesktop.login1.Manager.Reboot" boolean:true'
alias dbus-suspend='dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 "org.freedesktop.login1.Manager.Suspend" boolean:true'
alias dbus-hibernate='dbus-send --system --print-reply --dest=org.freedesktop.login1 /org/freedesktop/login1 "org.freedesktop.login1.Manager.Hibernate" boolean:true'

# Reset my wifi.
alias 'reset-wifi'='nmcli radio wifi off && sleep 3; nmcli radio wifi on'

alias tmux='TERM=xterm-256color tmux'
alias xcopy='xclip -i -selection clipboard'
