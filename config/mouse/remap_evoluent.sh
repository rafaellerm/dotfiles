#!/bin/sh

readonly DEVICE_ID=$(xinput list --id-only 'Kingsis Peripherals Evoluent VerticalMouse 4')
if [[ $? -eq 0 && -n "${DEVICE_ID}" ]]; then
    # Middle button -> right-button
    # Right-button -> middle-button
    xinput set-button-map "${DEVICE_ID}" 1 3 2 4 5 6 7 9 2 8
fi
