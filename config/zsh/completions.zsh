zstyle ':completion:*' completer _expand _complete _ignored
zstyle ':completion:*' matcher-list '' 'm:{[:lower:]}={[:upper:]}'
zstyle :compinstall filename '~/.zshrc'
zstyle ':completion:*' users root $USER

zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' accept-exact-dirs true
zstyle ':completion:*' hosts off

# Turn on cache.
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

autoload -Uz compinit
compinit -u

autoload bashcompinit
bashcompinit

unsetopt cdablevars
setopt AUTO_LIST
setopt AUTO_PARAM_SLASH

# Make completion work with some of my aliases.
compdef ok=okular
compdef sapt=apt-get
compdef mountssh=sshfs
