autoload colors; colors

export CLICOLOR=1

if [ -x /usr/bin/dircolors ]; then
    if [ -r ~/.dircolors ]; then
        eval "$(dircolors -b ~/.dircolors)"
    else
        eval "$(dircolors -b)"
    fi
fi

# colored completion - use my LS_COLORS
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

# Not sure if this falls into this category.
# Syntax highlighting for commands; to install, see
# https://github.com/zsh-users/zsh-syntax-highlighting
# highlight="$HOME/bin/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
# if [[ -e $highlight ]]; then
#     source $highlight
# fi
