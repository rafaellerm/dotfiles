# Left-hand prompt configuration.
PROMPT="%B%F{green}%n@%m%f%b"
PROMPT=$PROMPT"%F{white}:%f"
PROMPT=$PROMPT"%B%F{cyan}%~%f%b"
PROMPT=$PROMPT"%# "

# Create an indicator for vi-mode in the right-hand prompt. 
vim_normal_mode="%F{yellow}-- N%f"
vim_insert_mode="%F{cyan}-- I%f"
precmd() {
    RPROMPT="$vim_insert_mode"
}
function zle-line-init zle-keymap-select {
    RPROMPT="${${KEYMAP/vicmd/${vim_normal_mode}}/(main|viins)/${vim_insert_mode}}"
    zle reset-prompt
}
zle -N zle-line-init
zle -N zle-keymap-select

# History search with up/down keys.
autoload -U up-line-or-beginning-search
autoload -U down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search
