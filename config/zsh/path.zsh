function path_if_exists() {
  if [[ -d $1 ]]; then
    PATH="$1:$PATH"
  fi
}

# set PATH so it includes user's private bin if it exists
path_if_exists "$HOME/bin"
path_if_exists "$HOME/.local/bin"

# Add texlive directory.
path_if_exists /usr/local/texlive/2014/bin/x86_64-linux/
path_if_exists $HOME/code/texlive2015/bin/x86_64-linux/
