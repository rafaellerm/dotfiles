# Mostly copied from default ubuntu bash aliases.
alias ls='ls --color=auto -h'
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -CF'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history $HISTCMD |sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Sync environment from host to a tmux session. Should be called within the
# tmux session.
if [ -n "$TMUX" ]; then
  function refresh() {
      export "$(tmux show-environment | grep "^DISPLAY")"
  }
else
  function refresh() { }
fi

# Load bash aliases, if the file exists.
if [ -f ~/.bash_aliases ]; then
    source ~/.bash_aliases
fi
