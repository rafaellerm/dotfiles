HISTFILE=~/.histfile
HISTSIZE=20000
SAVEHIST=20000
setopt append_history
setopt hist_reduce_blanks

# setopt share_history
# "Up" will only look at the current shell's history.
# zle -N set-local-history 1
