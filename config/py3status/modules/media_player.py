#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
TODO: Write doc.
"""

import collections
import enum
import logging
import threading

import attr
import dbus
from dbus.mainloop import glib as main_loop
import gi.repository.GLib

logger = logging.getLogger('media_player')

_LEFT_CLICK = 1
_MIDDLE_CLICK = 2
_RIGHT_CLICK = 3


class Py3status:
    format = ('[{artist} - ][{title}] ['
              '[\\?color=degraded&if=state=2 \u23F8]|'
              '[\\?color=good&if=state=3 \u23F5]'
              ']')
    format_none = "no player running"
    play_pause_button = _LEFT_CLICK
    next_button = _MIDDLE_CLICK
    previous_button = _RIGHT_CLICK

    def post_config_hook(self):
        logger.info('post_config_hook called')
        self._listener = BusListener(self.py3)
        self._listener.start_listen()

    def kill(self):
        logger.info('kill called')
        self._listener.stop_listen()

    def media_status(self):
        status = max((p.state for p in self._listener.players()),
                     default=None,
                     key=lambda s: s.state)
        if not status:
            text = self.format_none
        else:
            text = self.py3.safe_format(
                self.format, attr_getter=lambda n: getattr(status, n))

        return {
            'full_text': text,
            'cached_until': self.py3.CACHE_FOREVER,
        }

    def on_click(self, event):
        # Find the first player who is playing.
        player = max((p for p in self._listener.players()),
                     default=None,
                     key=lambda s: s.state)
        if player is None:
            return
        try:
            if event['button'] == self.play_pause_button:
                player.toggle_state()
            elif event['button'] == self.next_button:
                player.next()
            elif event['button'] == self.previous_button:
                player.previous()
        except Exception as e:
            logger.warn('Error: %s', e)


@attr.s(frozen=True)
class PlayerState:
    """The current "state" of a player, with convenient accessors.

    The attributes and accessors defined here represent the "public" interface
    of the class: these are the attributes that are allowed in the format
    strings.
    """
    player_name = attr.ib()
    state_str = attr.ib()
    metadata = attr.ib()

    @property
    def title(self):
        return self.metadata.get('xesam:title')

    @property
    def album(self):
        return self.metadata.get('xesam:album')

    @property
    def artist(self):
        artists = self.metadata.get('xesam:artist')
        if isinstance(artists, str):
            return artists
        if isinstance(artists, collections.abc.Collection):
            return next(iter(artists))
        return artists

    @property
    def state(self):
        """The playing/not playing state of the player, as an enum."""
        return PlaybackStatus.from_string(self.state_str)

    def color(self, py3):
        st = self.status_enum
        if st == PlaybackStatus.PAUSED:
            return py3.COLOR_PAUSED or py3.COLOR_DEGRADED
        elif st == PlaybackStatus.PLAYING:
            return py3.COLOR_PLAYING or py3.COLOR_GOOD
        return py3.COLOR_STOPPED or py3.COLOR_BAD


@enum.unique
class PlaybackStatus(enum.IntEnum):
    """Allow sorting by status."""
    UNKNOWN = 0
    STOPPED = 1
    PAUSED = 2
    PLAYING = 3

    @classmethod
    def from_string(cls, value):
        if value == 'Stopped':
            return cls.STOPPED
        elif value == 'Paused':
            return cls.PAUSED
        elif value == 'Playing':
            return cls.PLAYING
        return cls.UNKNOWN


_MEDIAPLAYER_NAME = 'org.mpris.MediaPlayer2'
_MEDIAPLAYER_PATH = '/org/mpris/MediaPlayer2'
_PLAYER_NAME = 'org.mpris.MediaPlayer2.Player'
_PROPERTIES_INTERFACE = 'org.freedesktop.DBus.Properties'


class _PlayerListener:
    """A listener for a single player.

    This will load the current player state at startup, and listen to dbus for
    any PropertyChanged events.
    """
    def __init__(self, proxy, py3):
        self._proxy = proxy
        self._properties = dbus.Interface(proxy, _PROPERTIES_INTERFACE)
        self._player = dbus.Interface(proxy, _PLAYER_NAME)
        self._state = self._load_state()
        self._py3 = py3

    @property
    def state(self):
        return self._state

    def update(self):
        s = self._load_state()
        self._state = s
        return s

    def _load_state(self):
        identity = self._properties.Get(_MEDIAPLAYER_NAME, 'Identity')
        status = self._properties.Get(_PLAYER_NAME, 'PlaybackStatus')
        metadata = self._properties.Get(_PLAYER_NAME, 'Metadata')
        st = PlayerState(player_name=identity,
                         state_str=status,
                         metadata=metadata)
        return st

    def listen_changes(self, bus):
        self._listener = bus.add_signal_receiver(
            self._handle_changed,
            signal_name='PropertiesChanged',
            path='/org/mpris/MediaPlayer2',
            dbus_interface='org.freedesktop.DBus.Properties',
            bus_name=self._proxy.bus_name,
            sender_keyword='sender')

    def unlisten_changes(self):
        self._listener.remove()

    def _handle_changed(self, unused_interface, new_value, *args, sender=None):
        logger.info('Player %s sent signal: %s', sender, new_value)
        for key, value in new_value.items():
            if key == 'PlaybackStatus':
                self._state = attr.evolve(self._state, state_str=value)
            elif key == 'Metadata':
                self._state = attr.evolve(self._state, metadata=value)
        self._py3.update()

    def toggle_state(self):
        self._player.PlayPause()

    def next(self):
        self._player.Next()

    def previous(self):
        self._player.Previous()


class BusListener:
    """Listens to the global bus for media players being added or deleted.

    At startup, the existing players are also loaded.
    """
    def __init__(self, py3):
        dbus.mainloop.glib.threads_init()
        self._bus = dbus.SessionBus(mainloop=main_loop.DBusGMainLoop(
            set_as_default=True))

        self._name_listener = None
        self._eventloop_thread = None

        self._lock = threading.Lock()
        self._players = {}

        self._py3 = py3

    def start_listen(self):
        logger.info('start bus listener')
        self._name_listener = self._bus.add_signal_receiver(
            self._handle_name_owner_change,
            signal_name='NameOwnerChanged',
            path='/org/freedesktop/DBus',
            dbus_interface='org.freedesktop.DBus',
        )
        self._eventloop_thread = threading.Thread(target=self._start_loop,
                                                  daemon=True)
        self._eventloop_thread.start()
        logging.info('listener: %s', self._name_listener)
        self._listen_existing_players()

    def stop_listen():
        logger.info('stop bus listener')
        self._name_listener.remove()
        with self._lock:
            for player in self._players.values():
                player.unlisten_changes()
        self._loop.quit()

    def _listen_existing_players(self):
        with self._lock:
            for interface_name in self._bus.list_names():
                if interface_name.startswith(_MEDIAPLAYER_NAME):
                    try:
                        logger.info('Existing player: %s', interface_name)
                        proxy = self._bus.get_object(interface_name,
                                                     _MEDIAPLAYER_PATH)
                        p = _PlayerListener(proxy, self._py3)
                        self._players[interface_name] = p
                        p.listen_changes(self._bus)
                    except dbus.exceptions.DBusException as e:
                        logger.warn('DBus error initializing %s: %s',
                                    interface_name, e)

    def _start_loop(self):
        logger.info('Starting main loop')
        self._loop = gi.repository.GLib.MainLoop()
        self._loop.run()

    def _handle_name_owner_change(self, interface_name, old_owner, new_owner,
                                  *unused_args):
        logger.info('received new client: %s, old_owner: %s, owner: %s',
                    interface_name, old_owner, new_owner)

        if not interface_name.startswith(_MEDIAPLAYER_NAME):
            return

        is_deleted = (new_owner == '')
        if is_deleted:
            with self._lock:
                p = self._players.pop(interface_name)
            p.unlisten_changes()
        else:
            with self._lock:
                existing = self._players.pop(interface_name, None)
                if existing is not None:
                    existing.unlisten_changes()

                proxy = self._bus.get_object(interface_name, _MEDIAPLAYER_PATH)
                p = _PlayerListener(proxy, self._py3)
                self._players[interface_name] = p
                p.listen_changes(self._bus)

    def players(self):
        with self._lock:
            return list(self._players.values())


def _setup_logger():
    import sys
    # Allow using logging while in test mode.
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
        '%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)


if __name__ == "__main__":
    _setup_logger()
    from py3status.module_test import module_test
    module_test(Py3status)
