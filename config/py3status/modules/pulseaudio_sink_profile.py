#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Controls the profile for the first card.

Not sure what will happen if there are multiple cards. Also, this will only ever
cycle to profiles that have at least one input and one output, and are currently
marked as available.

TODO: Add formatting.
"""

import logging
import queue
import threading

import pulsectl

logger = logging.getLogger('pulseaudio_sink_profile')

_LEFT_CLICK = 1


class Py3status:
    next_button = _LEFT_CLICK

    def post_config_hook(self):
        self._pulse = pulsectl.Pulse('pa-card-profile-test')

        # This queue is mostly used as a condition variable, indicating that the
        # status should be updated.
        self._event_queue = queue.Queue(maxsize=1)
        self._event_queue.put_nowait(True)
        self._last_text = ''

        self._listener = _PulseListener(self.py3, self._event_queue)
        self._listener.start()

    def sink_status(self):
        try:
            # Only proceed if there is something new to get
            self._event_queue.get_nowait()
            logger.info('Updating text')

            profiles = []
            for card in self._pulse.card_list():
                available_profiles = [
                    p for p in card.profile_list if p.available
                ]
                if len(available_profiles) < 2:
                    continue
                card_name = card.proplist.get('device.description', card.name)
                profiles.append(
                    f'{card_name}: {card.profile_active.description} 🎛️')
                logger.info('Profiles: %s %s', card.name, _card_profiles(card))
            self._last_text = ', '.join(profiles)
        except queue.Empty:
            pass

        return {
            'full_text': self._last_text,
            'cached_until': self.py3.CACHE_FOREVER,
        }

    def kill(self):
        logger.info('kill called')

    def on_click(self, event):
        logger.info('on_click')
        if event['button'] != self.next_button:
            return

        for card in self._pulse.card_list():
            profiles = _card_profiles(card)
            logger.info('Profiles: %s', [p.description for p in profiles])
            if len(profiles) > 1:
                profile_names = [p.name for p in profiles]
                current_index = profile_names.index(card.profile_active.name)
                logger.info('current profile index: %s', current_index)
                new_index = (current_index + 1) % len(profiles)
                logger.info('new profile: %s, "%s"', new_index,
                            profiles[new_index].description)

                self._pulse.card_profile_set(card, profiles[new_index].name)
                return


def _card_profiles(card):
    available_profiles = {
        p.name: p
        for p in card.profile_list
        if (p.available and p.n_sinks and p.n_sources
            and 'surround' not in p.name)
    }
    available_profiles[card.profile_active.name] = card.profile_active
    return sorted(available_profiles.values(), key=lambda p: -p.priority)


class _PulseListener:
    def __init__(self, py3, event_queue):
        self._thread = None
        self._py3 = py3
        self._queue = event_queue

    def start(self):
        self._thread = threading.Thread(target=self._listen_events,
                                        daemon=True)
        self._thread.start()

    def _listen_events(self):
        with pulsectl.Pulse('pa-card-profile-listener') as pulse:
            pulse.event_mask_set('card')
            pulse.event_callback_set(self._process_cb)
            pulse.event_listen()

    def _process_cb(self, ev):
        try:
            self._queue.put_nowait(True)
        except queue.Full:
            logger.info('ALREADY SCHEDULED')
        self._py3.update()
        logger.info(ev)


def _setup_logger():
    import sys
    # Allow using logging while in test mode.
    logger.setLevel(logging.INFO)
    handler = logging.StreamHandler(sys.stderr)
    handler.setLevel(logging.INFO)
    formatter = logging.Formatter(
        '\n%(asctime)s %(name)-12s %(levelname)-8s %(message)s\n')
    handler.setFormatter(formatter)
    logger.addHandler(handler)


if __name__ == "__main__":
    _setup_logger()
    from py3status.module_test import module_test
    module_test(Py3status)
