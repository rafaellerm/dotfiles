#!/bin/sh

# Returns the current brightness value.
# TODO: See what happens when there's no monitor attached.
function get_current() {
    # ddcutil's output is of the form:
    # VCP 10 C 25 100
    # Where the value after "C" is the current value for the register. 10 is the brightness.
    local percentage=$(ddcutil --noverify --terse getvcp 10 |  awk '{ print $4 }')
    if [[ -z ${percentage} ]]; then
        return
    fi
    echo "{\"text\": ${percentage}, \"percentage\": ${percentage}, \"alt\": \"\" }"
}

function up() {
    ddcutil setvcp 10 + 10
}

function down() {
    ddcutil setvcp 10 - 10
}

case $1 in
    up) up ;;
    down) down ;;
    *) get_current ;;
esac
