set nocompatible

" At work, some things are different.
let at_work = filereadable(expand($HOME . '/.vim/work.vim'))

"""""""""""""""""""""""""""""""
" Plugins
"""""""""""""""""""""""""""""""

" Begin of Vim-plug config
let g:ale_emit_conflict_warnings = 0
call plug#begin('~/.vim/bundle')

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-scripts/The-NERD-Commenter'
Plug 'tmsvg/pear-tree'

Plug 'vim-scripts/Ambient-Color-Scheme'

Plug 'git://github.com/SirVer/ultisnips'
Plug 'git://github.com/honza/vim-snippets'
Plug 'git://github.com/google/vim-ft-bzl'
Plug 'git://github.com/majutsushi/tagbar'
Plug 'git://github.com/skywind3000/asyncrun.vim', { 'on': 'AsyncRun' }
Plug 'git://github.com/will133/vim-dirdiff', { 'on': 'DirDiff' }
Plug 'git://github.com/ConradIrwin/vim-bracketed-paste'
Plug 'tpope/vim-abolish'
Plug 'tpope/vim-unimpaired'

Plug 'dense-analysis/ale'

Plug 'junegunn/fzf', { 'dir': '~/bin/fzf-bin', 'do': './install --all' }
Plug 'junegunn/fzf.vim'

" Plug 'junegunn/vader.vim'

Plug 'prabirshrestha/async.vim'
Plug 'prabirshrestha/asyncomplete.vim'
Plug 'prabirshrestha/vim-lsp'
Plug 'prabirshrestha/asyncomplete-lsp.vim'
Plug 'prabirshrestha/asyncomplete-ultisnips.vim'
Plug 'prabirshrestha/asyncomplete-buffer.vim'
Plug 'tsufeki/asyncomplete-fuzzy-match', { 'do': 'cargo build --release' }

if has('nvim')
  Plug 'glacambre/firenvim', { 'do': { _ -> firenvim#install(0) } }
endif

" Some plugins will not be used at work.
if !at_work
    " Plug 'Valloric/YouCompleteMe'
    Plug 'vim-scripts/fugitive.vim'

    Plug 'google/vim-maktaba'
    Plug 'google/vim-glaive'
    Plug 'google/vim-codefmt'
endif

" End of Vundle config
call plug#end()

runtime macros/matchit.vim
let g:matchparen_timeout = 20
let g:matchparen_insert_timeout = 20

if !at_work
    call glaive#Install()
    Glaive codefmt plugin[mappings]
endif

"""""""""""""""""""""""""""""""
" Plugin configurations.
"""""""""""""""""""""""""""""""

" Configuration for airline.
set timeoutlen=30
set noshowmode
if has("gui_running")
    let g:airline_symbols = get(g:, 'airline_symbols', {})
    let g:airline_symbols.space = "\u3000"
endif
let g:airline_section_y=''
let g:airline_section_error=''
let g:airline_section_warning=''

" Configuration for NERD-Commenter.
let NERDSpaceDelims=1

" Configuration for YouCompleteMe.
if !at_work
    let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/third_party/ycmd/cpp/ycm/.ycm_extra_conf.py'
endif

" Configuration for Molokai.
let g:molokai_original=0
let g:rehash256=1

let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-v': 'vsplit' }

"""""""""""""""""""""""""""""""
" Key mappings
"""""""""""""""""""""""""""""""

" Remove search highlight
map <silent> <leader><cr> :nohlsearch<cr>

" Better way to move between windows
map <C-h> <C-W>h
map <C-j> <C-W>j
map <C-k> <C-W>k
map <C-l> <C-W>l

" More normal way of moving in wrapped lines
nmap j gj
nmap k gk

" More consistent Y behavior.
map Y y$

nmap <F5> :tabprev<CR>
nmap <F6> :tabnext<CR>
nmap <A-j> :tabprev<CR>
nmap <A-k> :tabnext<CR>
nmap j :tabprev<CR>
nmap k :tabnext<CR>

nmap <C-p> :Files<CR>
nmap <silent> <leader>t :TagbarOpenAutoClose<CR>
let g:AutoPairsShortcutFastWrap='<C-o>'

noremap <leader>f :YcmCompleter FixIt<CR>

" Ctrl+t will close the innermost HTML tag.
imap <C-t> </<C-x><C-o>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" VIM user interface
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Font for gvim.
set guifont=Droid\ Sans\ Mono

" Remove toolbar.
set guioptions-=T

" Set 7 lines to the cursor - when moving vertically using j/k.
set scrolloff=7

" Wild menu
set wildmenu
set wildignore=*.o,*~,*.pyc

" No neovim, don't mess with my terminal.
set mouse=

" Use word wrap
set wrap
set linebreak

" A buffer becomes hidden when it is abandoned
set hid

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

if has("gui_running")
    set spell
endif

" Makes search act like search in modern browsers
set incsearch

set hlsearch

set autoread

set ignorecase
set smartcase

" Show matching brackets when text indicator is over them
set showmatch
" How many tenths of a second to blink when matching brackets
set mat=2

" No annoying sound on errors
set noerrorbells
set novisualbell
set t_vb=
set tm=500

set number
set relativenumber
set laststatus=2
set ruler
set signcolumn=yes

colorscheme ambient

" Set a ruler for long lines.
set colorcolumn=+1
highlight ColorColumn ctermbg=17 guibg=gray9

"""""""""""""""""""""""""""""""
" Tab-related
"""""""""""""""""""""""""""""""
set smartindent
set expandtab
set shiftwidth=4
set tabstop=4
set softtabstop=4

" set foldlevelstart=20
setlocal foldmethod=indent
" Note, perl automatically sets foldmethod in the syntax file
autocmd Syntax c,cpp,java,vim,xml,html,xhtml setlocal foldmethod=syntax
autocmd BufWinEnter * normal zR

" Remove trailing whitespace on save for certain filetypes.
if !at_work
  fun! <SID>StripTrailingWhitespaces()
    let l = line(".")
    let c = col(".")
    %s/\s\+$//e
    call cursor(l, c)
  endfun
  autocmd FileType c,cpp,java,python,tex,vim,xml,html,xhtml autocmd BufWritePre <buffer> :call <SID>StripTrailingWhitespaces()
endif
command! RemoveWhitespace :%s/\s\+$//g

" Change snake case to camel.
command SnakeCase :s#\C\(\<\u[a-z0-9]\+\|[a-z0-9]\+\)\(\u\)#\l\1_\l\2#g | :nohlsearch


"""""""""""""""""""""""""""""""
" Syntastic
"""""""""""""""""""""""""""""""
let g:syntastic_python_python_exec = '/usr/bin/python3'
let g:syntastic_python_checkers = ['pyflakes']

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_check_on_open = 0
let g:syntastic_check_on_wq = 0

let g:syntastic_mode_map = {
    \ "mode": "active",
    \ "passive_filetypes": ["html"] }

"""""""""""""""""""""""""""""""
" UltiSnips
"""""""""""""""""""""""""""""""
let g:UltiSnipsEditSplit='vertical'
let g:UltiSnipsExpandTrigger="<C-h>"

let g:snips_author = ''
let g:snips_author_email = ''
let g:snips_author_homepage = ''


"""""""""""""""""""""""""""""""
" Netrw
"""""""""""""""""""""""""""""""
" Skip .gitignore'd files on listings.
if exists('*netrw_gitignore#Hide')
    let g:netrw_list_hide= netrw_gitignore#Hide()
endif

""""""""""""""""""""""""""""""
" Ag on visual selection mode
""""""""""""""""""""""""""""""
" vnoremap <silent>

""""""""""""""""""""""""""""""
" Visual mode related
""""""""""""""""""""""""""""""
" Visual mode pressing * or # searches for the current selection
vnoremap <silent> * :call VisualSelection('f')<CR>
vnoremap <silent> # :call VisualSelection('b')<CR>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Helper functions
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
function! VisualSelection(direction) range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$","","")

    if a:direction == 'b'
        execute "normal ?" . l:pattern . "^M"
    elseif a:direction == 'gv'
        call CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
    elseif a:direction == 'replace'
       call CmdLine("%s" . '/'. l:pattern . '/')
    elseif a:direction == 'f'
        execute "normal /" . l:pattern . "^M"
    endif

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Command aliases
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
command! W :write
command! Wa :wall
command! Q :quit
command! Qa :qall
if !at_work
    command! GoOutline :TagbarOpenAutoClose
endif


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Leave swap files out of my directory!
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set swapfile
set dir=~/tmp//,/var/tmp//,/tmp//

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-lsp.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
inoremap <expr> <cr>    pumvisible() ? asyncomplete#close_popup() : "\<cr>"
inoremap <c-space> <Plug>(asyncomplete_force_refresh)

nnoremap gd :LspDefinition<cr>
nnoremap gr :LspReferences<CR>
nnoremap <buffer> K <plug>(lsp-hover)

" Enable UI for diagnostics
let g:lsp_signs_enabled = 1           " enable diagnostics signs in the gutter
let g:lsp_diagnostics_echo_cursor = 1 " enable echo under cursor when in normal mode

let g:lsp_diagnostics_virtual_text_enabled = 0
let g:lsp_diagnostics_signs_enabled = 1

" Send async completion requests.
let g:lsp_async_completion = 1
" Enable UI for diagnostics
let g:lsp_signs_enabled = 1           " enable diagnostics signs in the gutter
" Automatically show completion options
let g:asyncomplete_auto_popup = 1

let g:lsp_diagnostics_enabled = 1
let g:lsp_diagnostics_echo_cursor = 1 " enable echo under cursor when in normal mode
let g:lsp_diagnostics_echo_delay = 1000

au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#ultisnips#get_source_options({
      \ 'config': {
      \   'show_source_kind': 1,
      \ },
      \ }))
au User asyncomplete_setup call asyncomplete#register_source(asyncomplete#sources#buffer#get_source_options({
      \ 'name': 'buffer',
      \ 'allowlist': ['*'],
      \ 'blocklist': ['go'],
      \ 'completor': function('asyncomplete#sources#buffer#completor'),
      \ 'config': {
      \    'max_buffer_size': 5000000,
      \  },
      \ }))

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-ale configs.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" https://github.com/dense-analysis/ale#faq-change-highlights
let g:ale_set_highlights = 0
let g:ale_virtualtext_cursor = 0
let g:airline#extensions#ale#enabled = 1

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" pear-tree stuff.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:pear_tree_smart_openers = 0
let g:pear_tree_smart_closers = 0
let g:pear_tree_smart_backspace = 0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Load stuff from work.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if at_work
    let s:work_path = expand($HOME . '/.vim/work.vim')
    if filereadable(s:work_path)
        exec 'source ' . s:work_path
    endif
endif
let s:commands_file = expand($HOME . '/.vim/commands.vim')
if filereadable(s:commands_file)
    exec 'source ' . s:commands_file
endif

filetype plugin indent on
