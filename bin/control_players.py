#! /usr/bin/env python3

import argparse
import enum
import itertools
import sys

import dbus


class Command(enum.Enum):
    PlayPause = 1
    Next = 2
    Previous = 3
    Status = 4

    @classmethod
    def from_string(cls, v):
        return cls[v]

    def __str__(self):
        return self.name


def main(args):
    parser = argparse.ArgumentParser(
        description='Control media players via DBus.')
    parser.add_argument('command', type=Command.from_string,
                        help='Command to execute.', choices=list(Command))
    players = list(PlayerProxy.get_players())

    command = parser.parse_args().command
    if command == Command.PlayPause:
        for p in itertools.chain(
                with_status(players, 'Playing'),
                with_status(players, 'Paused')):
            p.PlayPause()
    elif command == Command.Next:
        for p in with_status(players, 'Playing'):
            p.Next()
    elif command == Command.Previous:
        for p in with_status(players, 'Playing'):
            p.Previous()
    elif command == Command.Status:
        for p in players:
            print(p.Identity, p.PlaybackStatus, p.name)


def with_status(players, status):
    """Return which players in the list have the given status."""
    return [p for p in players
            if p.PlaybackStatus == status]


class PlayerProxy(object):
    """Wrapper over a player's proxy object."""
    _MEDIAPLAYER_NAME = 'org.mpris.MediaPlayer2'
    _PLAYER_NAME = 'org.mpris.MediaPlayer2.Player'
    _PROPERTIES_NAME = 'org.freedesktop.DBus.Properties'
    _MEDIAPLAYER_PATH = '/org/mpris/MediaPlayer2'
    _properties_map = {
        'Identity': _MEDIAPLAYER_NAME,
        'PlaybackStatus': _PLAYER_NAME}

    @classmethod
    def get_players(cls):
        """Get all available players."""
        sbus = dbus.SessionBus()
        for interface_name in sbus.list_names():
            if interface_name.startswith(cls._MEDIAPLAYER_NAME):
                proxy = sbus.get_object(interface_name, cls._MEDIAPLAYER_PATH)
                properties_proxy = dbus.Interface(proxy, cls._PROPERTIES_NAME)
                player_proxy = dbus.Interface(proxy, cls._PLAYER_NAME)
                yield PlayerProxy(player_proxy, properties_proxy, interface_name)

    def __init__(self, proxy, properties_proxy, name):
        self._proxy = proxy
        self._properties_proxy = properties_proxy
        self.name = name

    def __getattr__(self, attr):
        if attr in self._properties_map:
            interface = self._properties_map[attr]
            return self._properties_proxy.Get(interface, attr)
        else:
            return getattr(self._proxy, attr)


if __name__ == '__main__':
    main(sys.argv)
