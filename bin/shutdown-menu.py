#! /usr/bin/env python3

import collections
import subprocess

import dbus

_DMENU_COMMAND = ('rofi', '-dmenu', '-i')


def main():
    choices = collections.OrderedDict([
        ('Suspend', LoginManagerCommand('Suspend', True)),
        ('Shutdown', LoginManagerCommand('PowerOff', True)),
        ('Reboot', LoginManagerCommand('Reboot', True)),
        ('Hibernate', LoginManagerCommand('Hibernate', True)),
    ])

    dmenu_choose(choices)()


def dmenu_choose(dict_like):
    options = '\n'.join(dict_like.keys()).encode('utf-8')
    try:
        key = subprocess.check_output(
            _DMENU_COMMAND,
            input=options).decode('utf-8').strip()
        return dict_like[key]
    except (subprocess.CalledProcessError, KeyError):
        return dummy_function


def dummy_function():
    pass


class LoginManagerCommand(object):
    _LOGIN_MANAGER_PATH = '/org/freedesktop/login1'
    _LOGIN_MANAGER_BUS = 'org.freedesktop.login1'
    _LOGIN_MANAGER_INTERFACE = 'org.freedesktop.login1.Manager'

    def __init__(self, command, *args):
        self._command = LoginManagerCommand.login_manager().get_dbus_method(
            command, dbus_interface=self._LOGIN_MANAGER_INTERFACE)
        self._args = args

    def __call__(self):
        print(self._command)
        self._command(*self._args)

    _login_manager = None

    @classmethod
    def login_manager(cls):
        if cls._login_manager is None:
            cls._login_manager = dbus.SystemBus().get_object(
                cls._LOGIN_MANAGER_BUS,
                cls._LOGIN_MANAGER_PATH)
        return cls._login_manager


if __name__ == '__main__':
    main()
