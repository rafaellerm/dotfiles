#! /usr/bin/env python3

import collections
import i3ipc


def main():
    connection = i3ipc.Connection()

    active_workspaces = get_active_workspaces(connection)
    for workspace in active_workspaces:
        windows = get_floating_containers(workspace)
        arrange_bottom_right(workspace, windows)


def get_active_workspaces(connection):
    active_names = {w.name for w in connection.get_workspaces() if
                    w.visible}
    return [w for w in connection.get_tree().workspaces() if w.name in
            active_names]


def get_floating_containers(parent):
    for container in parent.descendants():
        if container.type == 'floating_con':
            yield from container.descendants()


def arrange_bottom_right(workspace, windows):
    print(workspace.name)
    print('\t', workspace.rect.x, workspace.rect.y,
          workspace.rect.width, workspace.rect.height)
    workspace_right = workspace.rect.x + workspace.rect.width
    workspace_bottom = workspace.rect.y + workspace.rect.height

    x_position = workspace_right
    for window in windows:
        print('\t', window.name)
        rect = window.parent.rect
        x_position -= rect.width
        y_position = workspace_bottom - rect.height
        window.command('move absolute position {} {}'.format(x_position,
                                                             y_position))

if __name__ == '__main__':
    main()
