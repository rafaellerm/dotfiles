let g:tex_indent_items=0

" Match all commands that end with 'ref' (case insensitive).
let LatexBox_ref_pattern = '\c\\\a*ref\*\?\_\s*{'

" Enable async compilation.
let g:LatexBox_latexmk_async = 1
" Open quickfix window only on errors, and don't steal focus.
let g:LatexBox_quickfix = 4
" Let's try continuous compilation.
let g:LatexBox_latexmk_preview_continuously = 1
" Turn off LaTeX-Box's own indent.
let g:LatexBox_custom_indent = 4

function! LatexJump()
let execstr = "silent !okular --unique ".LatexBox_GetOutputFile()."\\#src:".line(".").expand("%\:p").' &'
exec execstr
endfunction

command! LatexJump :call LatexJump()
nnoremap <Leader>f :call LatexJump()<CR>
