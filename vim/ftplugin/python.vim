setlocal tabstop=4
setlocal expandtab
setlocal shiftwidth=4
setlocal softtabstop=4

setlocal foldmethod=indent
setlocal foldtext=substitute(getline(v:foldstart),'\\t','\ \ \ \ ','g'
setlocal colorcolumn=+1
setlocal textwidth=80

" Stop messing with comment indentation
inoremap # X<BS>#


let g:EclimPythonValidate = 0
