function! PrintGivenRange() range
  echo "firstline ".a:firstline." lastline ".a:lastline
  echo "firstline contents" . getline(a:firstline)
  echo "lastline contents" . getline(a:lastline)
endfunction

command! -range PassRange <line1>,<line2>call PrintGivenRange()

vmap ,pr :PassRange<cr>

function! VisualAg() range
    let l:saved_reg = @"
    execute "normal! vgvy"

    let l:pattern = escape(@", '\\/.*$^~[]')
    let l:pattern = substitute(l:pattern, "\n$","","")

    call fzf#vim#ag(l:pattern)

    let @/ = l:pattern
    let @" = l:saved_reg
endfunction
command! -range Vag <line1>,<line2>call VisualAg()
