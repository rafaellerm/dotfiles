#!/usr/bin/env sh

# Enable shell glob goodness (only for bash).
if [ ! -z $BASH ]; then
    shopt -s globstar
fi

# Examine files in the console with colored output.
function cless() { pygmentize -g "$@" | less -i -R; }

function  gw() { (gwenview "$@"  2> /dev/null & );}
function ok() { (okular "$@" 2> /dev/null & );}
if ! [ -x "$(command -v ipython)" ]; then
    alias ipython=ipython3
fi

# Pdfcrop in batch, overwriting input files.
function pdfcropb() {
    for file in $@
    do
        echo $file
        output_file=$(tempfile)
        pdfcrop $file $output_file
        mv $output_file $file
    done
}

aliases_file="$HOME/.config/shell/aliases.sh"
if [[ -e $aliases_file ]]; then
  source $aliases_file
fi
