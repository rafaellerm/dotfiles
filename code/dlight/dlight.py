#! /usr/bin/env python3

import argparse
import binascii
import dataclasses
import hid
import queue
import struct
import contextlib
import threading
from typing import Optional


def main(opts):
    with ReportListener(0x18D1, 0x800B) as listener:
        if opts.brightness or opts.off or opts.on or opts.temperature:
            # If any mutation was requested
            status = listener.status
            if opts.brightness:
                status.brightness = opts.brightness
            if opts.off:
                status.on = False
            if opts.on:
                status.on = True
            if opts.temperature:
                # We expect a value between 0 and 100, but the lamp uses 0-24.
                value = int(opts.temperature // 100 * 34)
                status.temperature = value
            listener.status = status
        elif opts.continuous:
            for _ in range(20):
                st = listener.status_queue.get()
                print(st)
        else:
            print(listener.status_queue.get())


class ReportListener(contextlib.AbstractContextManager):
    """Wraps a thread that listens to the dLight's state."""

    def __init__(self, vendor, product):
        self._vendor = vendor
        self._product = product

        self._thread: threading.Thread = None
        self._device: hid.Device = None

        self._stopped = False

        self._buffer = bytearray()

        self._status: State = None
        self._status_available = threading.Event()
        self.status_queue = queue.Queue(10)

    def __enter__(self):
        self.start()
        self._device.__enter__()
        return self

    def start(self):
        self._device = hid.Device(self._vendor, self._product)
        self._thread = threading.Thread(target=self._read_loop)
        self._thread.start()

    def __exit__(self, exc_type, exc_value, traceback):
        self.stop()

    def stop(self):
        self._stopped = True
        # TODO: Send a poll, just to stop the read().
        if self._thread:
            self._thread.join(timeout=10)
            pass
        self._device.close()

    @property
    def status(self):
        if not self._status_available.is_set():
            send_poll(self._device)
        self._status_available.wait()
        return self._status

    @status.setter
    def status(self, status):
        self._device.write(status.to_bytes())

    def _read_loop(self):
        while not self._stopped:
            raw = self._device.read(64, timeout=1)
            if len(raw) < 2:
                # TODO: Log?
                continue
            msg_size = raw[0]
            if msg_size == 1 and raw[1] == 0x55:
                self._buffer.clear()
            self._buffer.extend(raw[1:])

            b = binascii.hexlify(self._buffer, " ")

            if len(self._buffer) >= 12:
                self._status = State.from_bytes(self._buffer[:12])
                self._status_available.set()
                try:
                    self.status_queue.put(self._status, block=False)
                except queue.Full:
                    self.status_queue.get()
                    self.status_queue.put(self._status, block=False)


@dataclasses.dataclass(slots=True)
class State:
    """Representation of a state report sent by dLight.

    Also contains the logic for parsing and serializing to/from the wire format.
    """

    _FORMAT = struct.Struct("!xx HxB ?BBB H")
    command_header: int = 0
    command: int = 0
    on: bool = False
    brightness: int = 0
    temperature: int = 0
    message_id: int = 0
    crc: int = 0

    @classmethod
    def from_bytes(cls, b):
        instance = cls(*cls._FORMAT.unpack(b))
        if instance.command_header != 0x0008:
            return None
        expected_crc = crc16_modem(b[:10])
        if instance.crc == expected_crc:
            return instance

    _PACK_FORMAT = struct.Struct("!?BBB")
    _PREAMBLE = b"\x55\xAA\x00\x08\x04\x10"

    def to_bytes(self):
        payload = self._PREAMBLE + self._PACK_FORMAT.pack(
            self.on, self.brightness, self.temperature, self.message_id
        )
        crc = _CRC_FORMAT.pack(crc16_modem(payload))
        # The first byte says the size of the rest of the message (including
        # CRC).
        return b"\x0c" + payload + crc


_CRC_FORMAT = struct.Struct("!H")

_POLL_COMMAND = b"\x55\xAA\x00\x05\x04\x11\x01"


def send_poll(device: hid.Device):
    crc = _CRC_FORMAT.pack(crc16_modem(_POLL_COMMAND))
    size = (len(_POLL_COMMAND) + len(crc)).to_bytes(1, "big")
    device.write(size + _POLL_COMMAND + crc)


def crc16_modem(buff):
    return binascii.crc_hqx(buff[:10], 0x00)


def parse_args():
    p = argparse.ArgumentParser("Control a table lamp from the command line.")

    setters = p.add_argument_group("Setter arguments")
    setters.add_argument(
        "--brightness", "-b", type=int, help="Brightness from 1 to 100."
    )
    setters.add_argument("--off", action="store_true", help="Turn light off.")
    setters.add_argument("--on", action="store_true", help="Turn light on.")
    setters.add_argument(
        "--temperature",
        "-t",
        type=int,
        help="Color temperature (in Kelvin, between 2600 and 6000)",
    )

    p.add_argument(
        "--continuous", "-p", action="store_true", help="Continuously print status"
    )
    return p.parse_args()


if __name__ == "__main__":
    main(parse_args())
