#!/usr/bin/env python3

# TODO: Make the device ID a parameter, it's apparently required in newer
# firmware versions.
from typing import Collection
import argparse
import contextlib
import dataclasses
import enum
import json
import socket
import struct


@dataclasses.dataclass
class State:
    on: bool
    brightness: int
    color_temperature: int

    @classmethod
    def from_json(cls, d):
        return cls(
            on=d.get("on"),
            brightness=d.get("brightness"),
            color_temperature=d.get("color", dict()).get("temperature"),
        )


class CommandType(enum.Enum):
    EXECUTE = 1
    QUERY_DEVICE_INFO = 2
    QUERY_DEVICE_STATES = 3


class ExecutionCmd:
    def to_dict(self):
        return {}


@dataclasses.dataclass
class OnCmd(ExecutionCmd):
    on: bool

    def to_dict(self):
        return {"on": self.on}


@dataclasses.dataclass
class BrightnessCmd(ExecutionCmd):
    brightness: int

    def to_dict(self):
        return {"brightness": self.brightness}

    def __post_init__(self):
        if not (0 <= self.brightness <= 100):
            raise ValueError("brightness must be betwen 0 and 100.")


@dataclasses.dataclass
class TemperatureCmd(ExecutionCmd):
    temperature: int

    def to_dict(self):
        return {"color": {"temperature": self.temperature}}

    def __post_init__(self):
        if self.temperature < 2600:
            raise ValueError("Minumum temperature is 2600")
        if self.temperature > 6000:
            raise ValueError("Maximum temperature is 6000")


class CustomEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, ExecutionCmd):
            return o.to_dict()
        return super(self).default(o)


class DLightConnection(contextlib.AbstractContextManager):
    def __init__(self, addr, port):
        self._socket = socket.socket(family=socket.AF_INET)
        self._socket.connect((addr, port))
        self._readback = self._socket.makefile(mode="rb")

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._socket.close()
        self._readback.close()

    def get_state(self):
        return State.from_json(
            self._send(CommandType.QUERY_DEVICE_STATES).get("states")
        )

    def info(self):
        return self._send(CommandType.QUERY_DEVICE_INFO)

    def status(self):
        return self._send(CommandType.QUERY_DEVICE_STATES)

    def execute(self, cmds: Collection[ExecutionCmd]):
        return self._send(CommandType.EXECUTE, commands=cmds)

    def _send(self, cmd_type: CommandType, **kwargs):
        cmd = {"commandType": cmd_type.name, "deviceId": "f2cZYIrw"}
        cmd.update(kwargs)
        msg = CustomEncoder().encode(cmd)
        print(msg)
        self._socket.send(msg.encode("utf8"))

        # Read the first 4 big-endian bytes that tell us the message size.
        header_size, *_ = struct.unpack("!L", self._readback.read(4))
        response = json.loads(self._readback.read(header_size))
        return response


def main(opts):
    if opts.demo:
        run_demo(opts)
        return
    if opts.info:
        with DLightConnection(opts.ip_address, opts.port) as d:
            print(d.info())
            return
    if opts.status:
        with DLightConnection(opts.ip_address, opts.port) as d:
            print(d.status())
            return

    cmds = []
    if opts.on:
        cmds.append(OnCmd(True))
    elif opts.off:
        cmds.append(OnCmd(False))
    if opts.brightness:
        cmds.append(BrightnessCmd(opts.brightness))
    if opts.color:
        cmds.append(TemperatureCmd(opts.color))
    with DLightConnection(opts.ip_address, opts.port) as d:
        if cmds:
            d.execute(cmds)
        print(d.get_state())


def run_demo(opts):
    import itertools
    import time

    with DLightConnection(opts.ip_address, opts.port) as d:
        for t in itertools.cycle(range(2600, 6000, 200)):
            d.execute([TemperatureCmd(t)])
            print(t)
            time.sleep(0.5)


def get_opts():
    parser = argparse.ArgumentParser(prog="DLight control")
    parser.add_argument("--ip_address", "-a", help="IP address for the light fixture.")
    parser.add_argument(
        "--port", "-p", default=3333, type=int, help="Port to connect to."
    )
    parser.add_argument("--demo", action="store_true", help="flip through colors")

    parser.add_argument("--on", action="store_true")
    parser.add_argument("--off", action="store_true")
    parser.add_argument("--brightness", "-b", type=int)
    parser.add_argument("--color", "-c", type=int)
    parser.add_argument(
        "--info", "-i", action="store_true", help="Only print device info"
    )
    parser.add_argument(
        "--status", "-s", action="store_true", help="Only print device status"
    )
    return parser


if __name__ == "__main__":
    main(get_opts().parse_args())
