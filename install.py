#!/usr/bin/env python3
"""Install symlinks to home folder.

Each file and directory in the root will be prepended with a dot, otherwise all
filenames and paths will be left untouched. Thus, a file in this repository
called "vimrc" will create a link to it in "~/.vimrc".

Links can be created without the dot -- for that, add the file path (relative to
the repository root) to a file called 'nonhidden.txt'.

Files can also be ignored by adding their paths to a file called
'ignored_files.txt'. This can be used on a per-machine basis to single-out files
that should not be synced.
"""

import os
import shutil

_NONHIDDEN_FILENAME = 'nonhidden.txt'
_IGNORE_FILENAME = 'ignored_files.txt'
_IGNORED_LIST = [
        __file__,
        _NONHIDDEN_FILENAME,
        _IGNORE_FILENAME,
        "README.md",
]

# Maintain on this list only files that actually exist.
_IGNORED_LIST = [f for f in _IGNORED_LIST if os.path.exists(f)]


def main():
    # Traverse the directory tree
    linker = LinkCreator()
    for directory, children, filenames in os.walk('.'):
        for filename in filenames:
            target_path = os.path.join(directory, filename)
            if not (is_ignored(target_path) or filename.startswith('.')):
                linker.create_link(target_path)

        # Ignore hidden directories in repository.
        children[:] = [x for x in children if not x.startswith('.')]


class LinkCreator(object):
    _home_dir = os.path.expanduser('~')

    def __init__(self, backup='dotfiles_old'):
        self._backup_dir = os.path.join(self._home_dir, backup)
        self._nonhidden = load_list_file(_NONHIDDEN_FILENAME)

        print('Nonhidden', self._nonhidden)

    def create_link(self, target):
        """Creates a link in the home directory pointing to the given target.

        Args:
            target: the link target, relative to pwd.
        """
        alias_path = os.path.join(self._home_dir, self.make_path(target))
        absolute_target = os.path.abspath(target)

        # Verify if files are already the same.
        if os.path.exists(alias_path):
            if os.path.samefile(alias_path, absolute_target):
                message = '"{}" and "{}" are already linked'.format(
                        alias_path, absolute_target)
                log_tagged('Skipping', message, bcolors.GREEN)
                return

            # Backup the file just for safety.
            self._backup_file(alias_path)

            # Remove the old file.
            log_tagged('Removing', alias_path, bcolors.BLUE)
            os.unlink(alias_path)

        # Finally, create the link.
        make_parent_dir(alias_path)

        log_tagged('Linking', '"{}" to "{}"'.format(alias_path,
            absolute_target), bcolors.YELLOW)
        os.symlink(absolute_target, alias_path)

    def _backup_file(self, absolute_file):
        """Copies the given absolute path to this instance's backup directory.

        Preserves the hierarchy starting from the home directory.

        Args:
            absolute_file: An absolute path to the file that must be backed up.
            It should be contained in the user's home directory.
        """
        relative_path = os.path.relpath(absolute_file, self._home_dir)
        backup_destination = os.path.join(self._backup_dir, relative_path)

        make_parent_dir(backup_destination)

        shutil.copy2(absolute_file, backup_destination)
        message = 'File {} copied to {}'.format(
                absolute_file, backup_destination)
        log_tagged('Copying', message, bcolors.BLUE)

    def make_path(self, path):
        """Appends a dot ('.') to the first segment in the given path, if
        necessary.

        This path may or may not contain a './' appended to it (which will be
        ignored).
        """
        if self.is_nonhidden(path):
            return os.path.relpath(path)
        else:
            return '.' + os.path.relpath(path)

    def is_nonhidden(self, path):
        for hidden_tree in self._nonhidden:
            if os.path.abspath(path).startswith(hidden_tree):
                return True
            common = os.path.commonprefix((hidden_tree, path))
            if os.path.exists(common) and os.path.samefile(hidden_tree, common):
                return True
        return False


def make_parent_dir(path):
    """Create the parent directory of path, if it does not exist."""
    destination_dir = os.path.dirname(path)
    if not os.path.exists(destination_dir):
        os.makedirs(destination_dir)


def load_list_file(filename):
    """Loads a file that contains a list of filenames, one per line.

    Empty lines, or lines starting with a '#', are ignored.
    Only filenames that exist will be returned.
    """
    def iter_file(list_file):
        for line in list_file:
            line = line.strip()
            if (not line.startswith('#')) and os.path.exists(line):
                yield os.path.abspath(line)
    try:
        with open(filename, 'r') as input_file:
            return list(iter_file(input_file))
    except FileNotFoundError:
        return []


def is_ignored(filename):
    for ignored in _IGNORED_LIST:
        if os.path.samefile(ignored, filename):
            return True
    return False


class bcolors:
    """Poor man's colorama module."""

    # From:
    # http://stackoverflow.com/questions/22886353/printing-colors-in-python-terminal
    BLUE = '\033[94m'
    GREEN = '\033[92m'
    YELLOW = '\033[93m'
    ENDC = '\033[0m'


def log_tagged(tag, message, color):
    print('{color}[{tag}]{clear} {message}'.format(
        color=color,
        tag=tag,
        message=message,
        clear=bcolors.ENDC))


if __name__ == '__main__':
    _IGNORED_LIST.extend(load_list_file(_IGNORE_FILENAME))
    main()
